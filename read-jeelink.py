__author__ = 'craigh'

import sys, traceback
import os

import logging
import time
import socket
import thread
import platform

#some optional logging choices
try:
    from logentries import LogentriesHandler
except ImportError:
    pass
try:
    import graypy
except ImportError:
    pass

from twisted.protocols.basic import LineReceiver
from twisted.internet import reactor
from twisted.internet.serialport import SerialPort
from twisted.python import usage
from serial import SerialException

import paho.mqtt.client as mqtt
from prometheus_client import Summary, Counter, Gauge, generate_latest

from twisted.python import log as twisted_log

from flask import Flask, jsonify, Response

app = Flask(__name__)

# instrumentation
CONTENT_TYPE_LATEST = str('text/plain; version=0.0.4; charset=utf-8')
SENSOR_SAMPLES = Counter('jeelink_samples_total', 'Number of samples processed', ['location'])
MQTT_SUBMIT_DURATION = Summary('jeelink_mqtt_submit_duration',
                               'Latency of submitting to mqtt')
MQTT_EXCEPTIONS = Counter('lacrosse_mqtt_submit_exceptions_total',
                          'Exceptions thrown submitting to mqtt', ['location','broker_url'])

# read env variables
mosquitto_url = os.getenv('MQTT_URL', 'mqtt')
mosquitto_port = os.getenv('MQTT_PORT', 1883)
mosquitto_user = os.getenv('MQTT_USER', None)
mosquitto_password = os.getenv('MQTT_PASSWORD', None)

serial_port = os.getenv('SERIAL_PORT', '/dev/ttyUSB0')
# let's use hostname
jeelink_location = os.getenv('JEELINK_LOCATION', platform.uname()[1])
gelf_url = os.getenv('GELF_URL', None)
logentries_key = os.getenv('LOG_ENTRIES_KEY', None)
logging_level = os.getenv('LOG_LEVEL', 'INFO')

log = logging.getLogger()

# if we have log configuration for log servers, add that, otherwise let's use basic loggin
isLogConfigInfo = False

if logentries_key is not None:
    log.addHandler(LogentriesHandler(logentries_key))
    isLogConfigInfo = True
if gelf_url is not None:
    handler = graypy.GELFHandler(gelf_url, 12201, localname='jeelink-mqtt', facility=jeelink_location)
    log.addHandler(handler)
    isLogConfigInfo = True

if not isLogConfigInfo:
    logging.basicConfig(level=logging.INFO)

log.setLevel(level=logging_level or logging.INFO)


class THOptions(usage.Options):
    optParameters = [
        ['baudrate', 'b', 57600, 'Serial baudrate'],
        ['port', 'p', serial_port, 'Serial port to use'],]


class ReadJeelink(LineReceiver):
    mqttc = mqtt.Client('jeelink/' + jeelink_location)
    if mosquitto_user:
        mqttc.username_pw_set(mosquitto_user, mosquitto_password )

    def write_to_mqtt(self, topic, value):
        log.info('Writing to topic: %s, val: %s' % (topic, str(value)))
        try:
            self.mqttc.connect(mosquitto_url, mosquitto_port, keepalive=1000)
            self.mqttc.publish(topic, str(value))
        except socket.error:
            log.warn('Could not connect to mosquitto, url:{0}'.format(mosquitto_url))
            MQTT_EXCEPTIONS.labels(broker_url=mosquitto_url,location=jeelink_location).inc()
        except:
            log.error('Error writing to mosquitto')

    def lineReceived(self, line):
        try:
            log.debug(line)
            msg = line.rstrip()
            if msg.startswith('D:') and len(msg) >= 11:
                label_dict = {"location":jeelink_location}
                SENSOR_SAMPLES.labels(**label_dict).inc()
                startTime = time.time()
                self.write_to_mqtt('private/jeelink/' + jeelink_location, line)
                MQTT_SUBMIT_DURATION.observe(time.time() - startTime)
        except (ValueError, IndexError):
            traceback.print_exc()
            log.error('Unable to parse data %s' % line)
            return
        except:
            log.error("Unhandled error in lineReceived")


def SerialInit():
    o = THOptions()
    try:
        o.parseOptions()
    except usage.UsageError, errortext:
        log.error('%s %s' % (sys.argv[0], errortext))
        log.info('Try %s --help for usage details' % sys.argv[0])
        raise SystemExit, 1

    baudrate = o.opts['baudrate']
    port = o.opts['port']
    log.info('About to open port %s' % port)
    s = SerialPort(ReadJeelink(), port, reactor, baudrate=baudrate)
    reactor.run()

log.info('Starting')


def flaskThread():
    app.run(host='0.0.0.0',port=5020)

thread.start_new_thread(flaskThread, ())

@app.route('/metrics')
def metrics():
    return Response(generate_latest(), mimetype=CONTENT_TYPE_LATEST)


observer = twisted_log.PythonLoggingObserver()
observer.start()
thread.start_new_thread(SerialInit())
