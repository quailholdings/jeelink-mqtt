A docker container which reads the usb port for output from an arduino sketch which is running on a jeelink, which is monitoring for lacrosse temperature settings.

This container will write the readings to mqtt, topic /private/temperature/{location}
Sample to run:

docker run -d --restart=always --name=jeelink-mqtt -e SENSORCONFIGFILE=/config.json -v /home/craigh/jeelink-mqtt-config/config.json:/config.json --device=/dev/ttyUSB0 craigham/rpi-jeelink-mqtt:latest

Sketch for the jeelink:
https://bitbucket.org/quailholdings/arduino-jeelink-lacrosse
