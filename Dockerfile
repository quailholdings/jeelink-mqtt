FROM hypriot/rpi-python:2.7.3
MAINTAINER craig

ARG git_commit
ARG version

RUN apt-get update && \
    apt-get -y install vim python-twisted python-pip && \
    apt-get clean

# Install Python requirements
ADD requirements.txt /tmp/requirements.txt
RUN pip install  --index-url=https://pypi.python.org/simple/ -r /tmp/requirements.txt

#ENV TZ=America/Los_Angeles
#RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Create runtime user
RUN useradd pi
RUN mkdir -p /home/pi
RUN usermod -a -G dialout pi
ADD read-jeelink.py /home/pi/read-jeelink.py
RUN chown -R pi /home/pi/
USER pi

LABEL git-commit=$git_commit
LABEL version=$version

EXPOSE 5020

CMD ["python","/home/pi/read-jeelink.py"]
